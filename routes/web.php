<?php



Route::get('/', function () {
    return "<h1>Primeira logica com laravel</h1>";
});


Route::get('/produtos', 'ProdutoController@lista');
Route::get('/produtos/mostra/{cod_prod}', 'ProdutoController@mostra');
Route::get('/produtos/remove/{cod_prod}', 'ProdutoController@remove');
Route::get('/produtos/alterar/{cod_prod}', 'ProdutoController@alterar');
Route::get('/produtos/novo', 'ProdutoController@novo');
Route::post('/produtos/adicionar', 'ProdutoController@adicionar');
Route::post('/produtos/atualizar', 'ProdutoController@atualizar');
Route::get('/produtos/json', 'ProdutoController@listarJson');