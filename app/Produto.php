<?php

namespace estoque;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
	protected $table = 'produto';
	 protected $primaryKey  = 'cod_prod';
	 public $timestamps = false;
	 protected $fillable = 
	 		array('desc_prod','valor_prod','tamanho');
    //
}
