<?php
namespace estoque\Http\Controllers;

use DB;
use Request;
use estoque\Produto;
use Validator;
use estoque\Http\Requests\ProdutoRequest;
class ProdutoController extends Controller{

	public function lista(){
		$produtos = Produto::all();
		return view('produtos.listagem')->with('produtos',$produtos);
	}

	public function listarJson(){
		$produtos = Produto::all();
		return response()->json($produtos);
	}

	

	public function mostra($id){
		$produto = Produto::find($id);
		if(empty($produto)) {
			return "Esse produto não existe";
		}
		return view('produtos.detalhes')->with('produto',$produto);
	}


	public function alterar($id){
		$produto = Produto::find($id);
		if(empty($produto)) {
			return "Esse produto não existe";
		}
		return view('produtos.formulario')->with('produto',$produto);
	}

	public function novo(){
		$produto = new Produto();
		return view('produtos.formulario')->with('produto',$produto);
	}

	public function remove($id){
		$produto = Produto::find($id);
		$produto->delete();
		return redirect()->action('ProdutoController@lista');

	}

	public function adicionar(ProdutoRequest $request){
		$params = $request->all();
		$produto = new Produto($params);
		$produto->cod_emp=1;
		$adicionar = array('cod_forn','cod_undbase','cod_sit_tributaria','minimo_estq_prod','qtd_saida','md5_prod','CFOP','CST_PIS','CST_COFINS','CST_IPI','CST_ICMS','ICMS','PIS','COFINS','IPI','NCM','cod_prod_erp','cst_contribuicoes','ex_tributario','cod_grupo_frac','marchar_prod'
	);
		foreach($adicionar as $add){
			$produto->$add = 0 ;
		}
		$produto->save();
		return redirect()->action('ProdutoController@lista')->withInput(Request::only('desc_prod'));
	}


	public function atualizar(){

		$params = Request::all();
		$produto =  Produto::find($params['cod_prod']);

		foreach($params as $idx=>$param){
			if(isset($produto->$idx)){
				$produto->$idx = $param;
			}
		}

		$produto->save();
		return redirect()->action('ProdutoController@lista');


	}

}