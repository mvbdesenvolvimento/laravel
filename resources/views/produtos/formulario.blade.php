@extends('layout.principal')
@section('conteudo')
<div class="container">
 @if(count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
    </ul>
   
  </div>
  @endif
  @if ($produto->cod_prod != '')
  <form action="/produtos/atualizar" method="POST">
    <input type="hidden" name="cod_prod" value="{{$produto->cod_prod}}">

    @else
    <form action="/produtos/adicionar" method="POST">

      @endif
      <input type="hidden" name="_token" value="{{csrf_token()}}">
      <h1> Novo produto </h1>
      <div class="form-group row">
        <label for="desc_prod" class="col-sm-2 col-form-label">Nome</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="desc_prod"  name="desc_prod" value="{{$produto->desc_prod}}">
        </div>
      </div>
      <div class="form-group row">
        <label for="val_venda_prod" class="col-sm-2 col-form-label">Valor produto</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="val_venda_prod" id="val_venda_prod"  value="{{$produto->val_venda_prod}}">
        </div>
      </div>
      <div class="form-group row">
        <label for="tamanho" class="col-sm-2 col-form-label">Tamanho</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="tamanho" id="tamanho"  value="{{$produto->tamanho}}">
        </div>
      </div>

      <div class="form-group row">
        <div class="col-sm-10">
         @if ($produto->cod_prod != '')
         <button type="submit" class="btn btn-primary">Alterar</button>

         @else
         <button type="submit" class="btn btn-primary">Cadastrar</button>

         @endif
       </div>
     </div>
   </form>
 </div>
 @stop 