@extends('layout.principal')
@section('conteudo')

@if(empty($produtos))
<div class="alert alert-danger">
	Você não tem nenhum produto cadastrado.
</div>
@else
<h1> Listagem de produtos</h1>
<div class="container"> 
	<table class="table table-striped table-bordered table-hover">
		<tr>
			<td>Nome</td>
			<td>Valor</td>
			<td>Custo</td>
			<td>Tamanho</td>
			<td></td>
			<td></td>
			<td></td>

		</tr>
		@foreach ($produtos as $key => $produto)
		<tr class="{{ $produto->val_venda_prod < 10 ? 'danger' : '' }}">
			<td>{{$produto->desc_prod}}</td>
			<td>{{$produto->val_venda_prod}}</td>
			<td>{{$produto->val_custo_prod}}</td>
			<td>{{$produto->tamanho}}</td>
			<td><a href="/produtos/mostra/{{$produto->cod_prod}}"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a></td>
			<td><a href="/produtos/remove/{{$produto->cod_prod}}"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
			<td><a href="/produtos/alterar/{{$produto->cod_prod}}"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a></td>
		</tr>
		@endforeach
	</table>
</div>


@if((old('desc_prod')))
<div class="container">
	<div class="alert alert-success">
	{{old('desc_prod')}} ADICIONADO COM SUCESSO
</div>
</div>
@endif
@endif

@stop